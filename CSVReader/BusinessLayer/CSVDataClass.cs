﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
   public class Data
    {
        public List<string> FirstName { get; set; }
        public List<string> LastName { get; set; }
        public List<string> Address { get; set; }
        public List<string> PhoneNumber { get; set; }
    }
}
