﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace BusinessLayer
{
    public class ReadCSV
    {
       public  List<string> FirstName { get; set; }
       public  List<string> LastName { get; set; }
       public  List<string> Address { get; set; }
       public  List<string> PhoneNumber { get; set; }

       public List<string[]> GetCSVData(string path)
       {
           List<string[]> csvData = new List<string[]>();
          List<Data> Alldata=new List<Data>(); 
           var FirstName = new List<string>();
           var LastName = new List<string>();
           var Address = new List<string>();
           var PhoneNumber = new List<string>();
          
             try
               {
                    //see if file exists before anything
                if(File.Exists(path))
                {
                    //start reading file
                    using (var file=new StreamReader(path))
                    {
                        //read only if data still exists in the csv file
                        while(!file.EndOfStream)
                        {
                            var fileSplits = file.ReadLine().Split(',');
                            FirstName.Add(fileSplits[0]);
                            LastName.Add(fileSplits[1]);
                            Address.Add(fileSplits[2]);
                            PhoneNumber.Add(fileSplits[3]);
                        }
                    
                    }

                    csvData.Add(FirstName.ToArray());
                    csvData.Add(LastName.ToArray());
                    csvData.Add(Address.ToArray());
                    csvData.Add(PhoneNumber.ToArray());
                }
                else
                {
                    return null;
                }
                return csvData;
               }
               catch(Exception)
               {
                   return null;
               }
          
          
           
       }

       public List<string> GetColumnFirstName(List<string[]> csvData)
       {
           string[] FirstNames = csvData[0];
           var FirstNamesList = FirstNames.ToList();
            FirstNamesList.RemoveAt(0);
           return FirstNamesList;
       }

       public List<string> GetColumnLastName(List<string[]> csvData)
       {
           string[] LastName = csvData[1];
           var LastNameList = LastName.ToList();
           LastNameList.RemoveAt(0);
           return LastNameList;
       }

       public List<string> GetColumnAddress(List<string[]> csvData)
       {
           string[] Address = csvData[2];
           var AddressList = Address.ToList();
           AddressList.RemoveAt(0);
           return AddressList;
       }
       public List<string> GetColumnPhonenumbers(List<string[]> csvData)
       {
           string[] Phonenumbers = csvData[3];
           var PhonenumbersList = Phonenumbers.ToList();
           PhonenumbersList.RemoveAt(0);
           return PhonenumbersList;
       }

       public static bool WriteToTextFile1(List<string> LastName,List<string> FirstNames)
       {
           try
           { 
            bool success = false;
            int count = 0;
           var desktopPath = System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

           List<string> duplicatesCount=new List<string>();
           List<string> Names = new List<string>();

           var FirstLastName = new Dictionary<string, string>();
           var LastNamedict = new Dictionary<string, int>();
               
           //write list of first and last names
           foreach (var fname in FirstNames)
           {
               Names.Add(string.Format("{0} {1}", FirstNames[count], LastName[count]));           
               count++;
           }
           
           Names.Sort();
           Names.Reverse();
           System.IO.File.WriteAllLines(desktopPath + @"\" + "output.txt", Names);
           System.IO.File.AppendAllText(desktopPath + @"\" + "output.txt", "------------------------------\r\n");
           

           foreach (var name in LastName)
           {
               if (LastNamedict.ContainsKey(name))
                   LastNamedict[name]++;
               else
                   LastNamedict[name] = 1;
           }

           var dictDescendingLastnames = from pair in LastNamedict
                       orderby pair.Value descending
                       select pair;

           var dictAscendingKeys = from pair in dictDescendingLastnames
                                      orderby pair.Key ascending
                                      select pair;
           
           foreach (var pair in dictAscendingKeys)
           {
               duplicatesCount.Add(string.Format("{0},{1}", pair.Key, pair.Value));
           }

                

           duplicatesCount.Reverse();
             
            System.IO.File.AppendAllLines(desktopPath + @"\" + "output.txt",duplicatesCount);
           
           
           success = true;
           return success;
           }
           catch(Exception)
           {
               return false;
           }
          

       }

       public static bool WriteToTextFile2(List<string> Addresses)
       {
           try
           {
               bool success = false;
               var desktopPath = System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
               List<string> AscAddresses = new List<string>();
               var dict = new Dictionary<string, int>();

               foreach (var address in Addresses)
               {
                   string Addstr = Regex.Replace(address, @"\d", "");
                   int Addnumber =Convert.ToInt32(Regex.Replace(address, "[^0-9]+", string.Empty));
                                  
                   dict[Addstr] = Addnumber;
               
               }

               var dictAscByKeys = from pair in dict
                                          orderby pair.Key ascending
                                          select pair;
               //assign values into an array
               foreach (var pair in dictAscByKeys)
               {
                   AscAddresses.Add(string.Format("{0} {1}",pair.Value,pair.Key));
               }

                  System.IO.File.WriteAllLines(desktopPath + @"\" + "output2.txt", AscAddresses);
               success = true;
               return success;
           }
           catch (Exception)
           {
               return false;
           }


       }

    }


}
