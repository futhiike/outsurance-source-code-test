﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using BusinessLayer;
using ConsoleApp;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {

        }

        [TestMethod]
        public void WriteToTextFile1Test()
        {
            List<string> LastName=new List<string>();
            List<string> FirstNames = new List<string>();

            FirstNames.Add("Futie");
            FirstNames.Add("Name1");
            FirstNames.Add("Name2");
            FirstNames.Add("Name3");
            FirstNames.Add("Name4");
            FirstNames.Add("Name5");
            LastName.Add("Blose");
            LastName.Add("SurName1");
            LastName.Add("SurName2");
            LastName.Add("SurName3");
            LastName.Add("SurName4");
            LastName.Add("SurName5");


            var isSuccessfull = ReadCSV.WriteToTextFile1(LastName, FirstNames);

            Assert.AreEqual(true, isSuccessfull);
        }

        
             [TestMethod]
        public void WriteToTextFile2Test()
        {
            List<string> Addresses = new List<string>();

            Addresses.Add("Street address1");
            Addresses.Add("11 address");
            Addresses.Add("22 address2");
            Addresses.Add("33 address3");
            Addresses.Add("44 address4");
            Addresses.Add("55 address5");

            var isSuccessfull = ReadCSV.WriteToTextFile2(Addresses);

            Assert.AreEqual(true, isSuccessfull);
        }


    }
}
