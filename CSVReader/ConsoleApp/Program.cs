﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using BusinessLayer;

namespace ConsoleApp
{
   public class Program
    {
        static void Main(string[] args)
        {
            CallConsole();           

        }
       private static void CallConsole()
       {
            //class object created to call method
            ReadCSV CSVobj = new ReadCSV();
            string extension = ".csv";

            Console.WriteLine("Please place file on the desktop and enter file name:");
          var desktopPath=  System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            var FileName = Console.ReadLine();
            FileName += extension;
            desktopPath += @"\" + FileName;

            if (File.Exists(desktopPath))
            {
                //Get arraylist of all data in CSV file
         List<string[]> CsvData= CSVobj.GetCSVData(desktopPath);
         try
         {
            if (CsvData != null || CsvData.Count>0)
                {
               
                    List<string> LastNames = CSVobj.GetColumnLastName(CsvData);
                    List<string> FisrtNames = CSVobj.GetColumnFirstName(CsvData);
                    bool success = ReadCSV.WriteToTextFile1(LastNames, FisrtNames);
                  if (success)
                {
                    Console.WriteLine("Succefully written to output.txt  file on the desktop");
                    Console.WriteLine();
                    Console.WriteLine(" Continue Y/N");
                 string answer = Console.ReadLine();
                      if(answer.ToUpper() !="Y")
                      {
                          Environment.Exit(0);
                      }
                }

                //Write second textfile
                List<string> Addresses = CSVobj.GetColumnAddress(CsvData);
                bool complete = ReadCSV.WriteToTextFile2(Addresses);

                if(complete){
                    Console.WriteLine("Succefully written Addresses to output2.txt  file on the desktop");
                    Console.WriteLine();
                }
                Console.WriteLine(" Continue Y/N");
                string result = Console.ReadLine();
                if (result.ToUpper() != "Y")
                {
                    Environment.Exit(0);
                }
                else
                {
                    CallConsole();
                }


                    
                }
            
         }
         catch(Exception ex)
         {
             Console.WriteLine("Error :"+ex.Message.ToString());
             Console.WriteLine();
             CallConsole();
         }
               
                //return required column only
        
            }
            else
            {
                Console.WriteLine("Incorrect file name entered please retry:");
                Console.WriteLine();
                CallConsole();
            }
       }
    }
}
